module Opdracht2
    
    where 

        import Data.List
        import Data.Char
        ---Opdracht 1
        -- Schrijf het datatype Boek. Dit datatype bevat drie velden:
        -- • Prijs: De prijs van het boek.
        -- • Titel: De titel van het boek.
        -- • Auteur: De schrijver van het boek.
        -- Gebruik type synoniemen om de code leesbaar te houden.


        data Book = Book Price Title Author
            deriving (Show)

        type Price = Int
        type Title = String
        type Author = String


        -- Opdracht 2
        -- Leidt je Boek af van de typeclasses Eq en Ord. We willen boeken onderling
        -- kunnen vergelijken (Eq) en op volgorde kunnen zetten (Ord). Voorwaarden:
        -- • Twee boeken zijn aan elkaar gelijk als alle velden gelijk zijn.
        -- • Boeken dienen alfabetisch gesorteerd te kunnen worden op de titel van
        -- het boek. Prijs en auteur intesseren ons niet.
        instance Eq Book where
            (Book price1 title1 author1) == (Book price2 title2 author2) = (price1==price2) && (title1==title2) && (author1==author2)
            _ /= _ = False

        instance Ord Book where
            compare (Book _ title1 _) (Book _ title2 _) = compare title1 title2


        ---Opdracht 3
        -- Maak een lijst van tenminste vijf boeken. Test of je implementatie van de EQ
        -- en Ord typeclasses correct werkt door:
        -- • enkele boeken met elkaar te vergelijken.
        -- • de lijst met boeken te sorteren middels het gebruik van de sort functie.
        -- Deze functie zit in de library Data.List.

        book1 = Book 12 "Schaum's Outline of Programming with Java" "John R. Hubbard"
        book2 = Book 12 "The Subtle Art of Not Giving a F*ck" "Mark Manson"
        book3 = Book 87 "A Dance with Dragons" "Sjors R. R. Martijn" 
        book4 = Book 2 "Donald Duck" "Walt Disney"
        book5 = Book 8 "H. Potter" "J. R. R. Tolkien"
        book6 = Book 8 "H. Potter" "J. R. R. Tolkien" 
        -- troll

        bookList = [book1, book2, book3, book4, book5, book6]

        -- Tests:
        ---book1==book2 geeft False
        ---book1==book1 geeft True
        ---sort bookList geeft gesorteerde lijst list



        -- Opdracht 4
        -- Schrijf het datatype Box a. Dit datatype is algebra¨ısch. Een box:
        -- • is Leeg.
        -- • bevat iets, waarvan we van te voren niet weten wat het is.

        data Box a = EmptyBox
                | Box a 
                deriving (Show)


        --Opdracht 5
        -- Schrijf een functie die alle boeken in een gegeven lijst in een Box inpakt. Schrijf
        -- ook een functie die het omgekeerde doet en, so to speak, alle boeken weer
        -- uitpakt.
        putBooklistInBox:: [Book] -> Box [Book]
        putBooklistInBox booklist = Box booklist

        getBooklistFromBox:: Box [Book] -> [Book]
        getBooklistFromBox (Box booklist) = booklist

        ------------------------------------------------------------------------

        ---Opdracht 6
        -- Schrijf het datatype Zak. Ook dit datatype is algebra¨ısch. Verder gelden dezelfde voorwaarden als bij het Box type: Een zak bevat iets of is Leeg
        data Bag a = EmptyBag
                | Bag a
                deriving (Show)

        --Opdracht 7
        -- Leidt zowel het datatype Box als het datatype Zak af van de Functor typeclass
        -- en implementeer de functie fmap voor beide datatypen.

        instance Functor Box where
            fmap f EmptyBox = EmptyBox
            fmap f (Box a) = Box (f a)

        instance Functor Bag where
            fmap f EmptyBag = EmptyBag
            fmap f (Bag a) = Bag (f a)

        -- Opdracht 8
        -- Schrijf enkele oneliners:
        -- • Elk boek in de boekenlijst wordt ingepakt in een Zak en al deze zakken
        -- worden, per stuk, ingepakt in een Box
        -- • Maak een willekeurige lijst getallen. Stop elk getal in een Box. We hebben
        -- nu een lijst met boxes, waarbij in elke Box een getal zit.
        numberList = [0..100]

        putBooksInBagsAndBoxes:: [Book] -> [Box (Bag Book)]
        putBooksInBagsAndBoxes booklist =  map (fmap inBag) boxedList
                where
                    inBag a = Bag a
                    inBox a = Box a
                    boxedList = map inBox booklist
        
        putNumbersInBoxes:: Num a => [a] -> [Box a] 
        putNumbersInBoxes numlist = map inBox numlist
                where 
                    inBox a = Box a



        --Opdracht 9
        -- Schrijf het datatype List. Dit datatype is geparameteriseerd en definieert recursief een list.
        -- • Leidt de List af van de Functor typeclass.
        -- • Vul de List met dozen met getallen. Gebruik daarvoor de foldr of
        -- foldl functie.
        -- • Gebruik de functionaliteit van Functor om een met Box gevulde list te
        -- vervangen door een met Zak gevulde list.

        data List a = Head a (List a) | EmptyList 
                deriving (Show)
                
        instance Functor List where
            fmap f EmptyList = EmptyList
            fmap f (Head a tail) = Head (f a) (fmap f tail)        

        push :: a -> List a -> List a --Push een item naar een List
        push a EmptyList = Head a EmptyList
        push a (Head h tail) = Head a (Head h tail)

        pushList::List a -> [a] -> List a   --Push een List van items naar een List
        pushList EmptyList list = foldr push EmptyList list
        pushList (Head h tail) list = foldr push (Head h tail) list

        fillList lijst = foldr push EmptyList lijst 
            
        convertBoxToBag:: Box a -> Bag a
        convertBoxToBag (Box a) = Bag a

        convertBoxList::List (Box a) -> List (Bag a)   -- Lijst van dozen naar lijst van zakken verwisselen
        convertBoxList EmptyList = EmptyList
        convertBoxList list = fmap convertBoxToBag list
