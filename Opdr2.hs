module Opdr2 where
import Data.Char

--opdr 1a
euclid::Integer->Integer->Integer
euclid x y
    |mod x y == 0 = y
    |otherwise = euclid (mod y x) x

--opdr 1b
egcd::Integer->Integer->(Integer,Integer,Integer)
egcd 0 b = (b, 0, 1)
egcd a b =
    let (g, s, t) = egcd (b `mod` a) a
    in (g, t - (b `div` a) * s, s)

mid::(a,b,c)->b
mid (_,b,_) = b

megcd::Integer->Integer->Integer
megcd a b = abs(mid(egcd a b))

--opdr 2
rsakeygen::Integer->Integer->(Integer,Integer,Integer)
rsakeygen p q =
    let m=p*q
        m'=(p-1)*(q-1)
        e = head $tail [e|e<-[1..m'], euclid e m'==1]
        d = megcd e m'
    in (e, d, m) 

--opdr3a
rsaencrypt::(Integer,Integer)->Integer->Integer
rsaencrypt (e, m) a =
    let c = (a^e) `mod` m
    in c

--opdr3b
rsadecrypt::(Integer,Integer)->Integer->Integer
rsadecrypt (d, m) c =
    let a = (c^d) `mod` m
    in a

--opdr4
toascii::Char->Int
toascii a = ord a

fromascii::Int->Char
fromascii a = chr a

naarascii::Char->Int
naarascii a = fromEnum a

vanascii::Int->Char
vanascii a = toEnum a

--opdr5
-- Bob generates a key pair; his public & private key
-- Bob publishes his public key and Alice fetches it.
-- Alice generates a temporary symmetric key and uses Bob’s public key to encrypt the message to send it to Bob.
-- Bob then uses his private key to unlock his copy of the symmetric key and decrypt the message
