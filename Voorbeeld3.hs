module Voorbeeld3 where
import Data.Char
import Data.List

-- Opdrachten 3 Func. Prog.

--1a: differentieren
----- f = functie  -> is een functie die een double als input neemt (x prob), en een double als output geeft (y prob)
----- p = precizie
----- x = op punt x 
--Differentieer d.m.v. differentiequotiënt
differentieer::(Double->Double)->Double->Double->Double
differentieer f p x = (f(x + p) - f(x)) / p
-- https://wiki.haskell.org/Functional_differentiation
-- in prelude:
-- let func a = a^2 + 6*a + 1 
-- differentieer func (1/10000) 3
-- differentiateTest::Double->Double->Double
-- differentiateTest p x =
--     let func a = a^2 + 6*a + 1
--         fx = func x
--         fxp = func (x+p)
--     in ( fxp - fx ) / ( (x+p)-x )

--1b
-- Integreer d.m.v. riemannintegratie
-- Deze functie integreert de functie f op het interval a, b met een precisie p.
integreer::(Double->Double)->Double->Double->Double->Double
integreer f a b p =
    foldr (\l r->(dx * (f(a + (l * dx)))) + r) 0 [0..p-1]
    where dx = (b-a)/p

--2	
 --Scan een lijst voor dubbele entries
dubbelen::(Ord a) => [a]->[a]
dubbelen s = sort(nub((s) \\ (nub s)))

---3: poker (hogere orde functies)
--Stenen
s = [1..6]
stenen = [[a,b,c,d,e]|a<-s,b<-s,c<-s,d<-s,e<-s]
totaalstenen = length (stenen)       

-- Onderstaande functie retourneert het aantal voorkomens van c in een lijst:
count::Integer->[Integer]->Integer
count c [] = 0                  -- lege lijst
count c (x:xs)                  
    |c==x = 1 + (count c xs)    --als huidige element in lijst c is (wat we zoeken), 1 +, and door de rest van de lijst tellen
    |otherwise = count c xs     --anders blijft count hetzelfde

--Onderstaande functie converteert een lijst in een aantal tuples met voorkomens:
convert list = ([a,b,c,d,e,f],list) 
    where
        a = count 1 list
        b = count 2 list
        c = count 3 list
        d = count 4 list
        e = count 5 list
        f = count 6 list

zelfde::Integer->[[Integer]]->[[Integer]]
zelfde x list = filter (elem x) (map fst (map convert stenen))

poker = (fromIntegral (numberPoker) ) / (fromIntegral (totaalstenen) )
    where
        numberPoker = length (listPoker)
        listPoker = zelfde 5 stenen

fourOfAKind = (fromIntegral (numberFourOfAKind) ) / (fromIntegral (totaalstenen) )
    where
        numberFourOfAKind = length(listFourOfAKind)
        listFourOfAKind = zelfde 4 stenen

fullHouse = (fromIntegral (numberFullHouse) ) / (fromIntegral (totaalstenen) )
    where
        numberFullHouse = length(listFullHouse)
        listFullHouse = filter (elem 2) listThree       --three of a kind + two of a kind
        listThree = zelfde 3 stenen
--(exclusief fullhouses)
threeOfAKind = chanceThree - fullHouse
    where 
        chanceThree = (fromIntegral (numberThree) ) / (fromIntegral (totaalstenen) )
        numberThree = length(listThree)
        listThree = zelfde 3 stenen

twoPairs = (fromIntegral (numberTwoPairs) ) / (fromIntegral (totaalstenen) )
    where
        numberTwoPairs = length(listTwoPairs)
        listTwoPairs = filter (elem 2) (map fst (map convert listPairs))        
        listPairs = zelfde 2 stenen

-- filter de twopairs en fullhouses eruit
pair = chancePair - twoPairs - fullHouse
    where
        chancePair = (fromIntegral (numberPairs) ) / (fromIntegral (totaalstenen) )
        numberPairs = length(listPairs)
        listPairs = zelfde 2 stenen

straight = (fromIntegral (numberStraight) ) / (fromIntegral (totaalstenen) )
    where 
    numberStraight = length(listStraight)
    listStraight = listFiveOnes
    listFiveOnes = filter (elem 5) (map fst (map convert listOnes))     -- 5 stenen met ander resultaat, maar controleert niet de volgorde
    listOnes = zelfde 1 stenen

-- wat overblijft
bust = 1 - poker - fourOfAKind - fullHouse - threeOfAKind - twoPairs - pair - straight
