module Opdr1 where

--opdr 1a
faca::Int->Int
faca 0=1
faca x = x * faca (x-1)

--opdr 1b 
facb::Int->Int
facb x
    |x==1 = 0
    |otherwise = x * facb (x-1)

--opdr 2a
nulpa::Double->Double->Double->[Double]
nulpa a b c = [(-b+sqrt((b^2)-4*a*c))/(2*a),(-b-sqrt((b^2)-4*a*c))/(2*a)]

--opdr 2b
nulpb::Double->Double->Double->[Double]
nulpb a b c
    |a==0 = []
    |disc<0 = []
    |disc==0 = [formulaa]
    |disc>0 = [formulaa, formulab]
    where disc = ((b^2)-4*a*c)
          formulaa = ((-b-sqrt(disc))/2*a)
          formulab = ((-b+sqrt(disc))/2*a)
--opdr 2c
dobbelsteena::Int
dobbelsteena = length[(x,y,z)|x<-[1..6],y<-[1..6],z<-[1..6], mod (x+y+z) 5 == 0]

--opdr 2d
dobbelsteenb::Int->Int
dobbelsteenb n = length[(x,y,z)|x<-[1..6],y<-[1..6],z<-[1..6], mod (x+y+z) n == 0]
