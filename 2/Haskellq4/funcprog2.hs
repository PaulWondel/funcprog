module FuncProg2
    where

        import Data.List
	import Data.Char

	---Opdracht 1
        ---1: Opdracht 1.1
		-- Definieer het datatype Geofig. Dit datatype stelt een geometriche figuur voor.
		-- Enkele eigenschappen:
		-- • Het datatype is algebra¨ısch: Er bestaan dus verschillende waarden (value
		-- constructoren) van. Deze zijn:
		-- – Vierkant: parameters: Lengte (=de lengte van een zijde)
		-- – Rechthoek: parameters: Lengte, Breedte
		-- – Driehoek: parameters: Lengte (=de lengte van een zijde), de driehoek is gelijkzijdig.
		-- – Cirkel: parameters: Straal
		-- • Elke geometrische figuur heeft een kleur. Mogelijke kleuren zijn: Rood, Blauw, Geel
		-- Ook voor de mogelijke kleuren wordt een nieuw datatype gedefinieerd,
		-- d.w.z. een enumeratietype.


      
        data Geofig = Square { 
                        geocolour::Colour, 
                        geolength::Float 
                        }
                    | Rectangle { 
                        geocolour::Colour,
                        geolength::Float,
                        geowidth::Float 
                        }
                    | Triangle {
                        geocolour::Colour,
                        geolength::Float
                        } 
                    | Circle {
                        geocolour::Colour,
                        georadius::Float
                        }
                    deriving (Eq, Ord, Show)

        data Colour = Red
                    | Blue
                    | Yellow
                    deriving (Eq, Ord, Show)


        ---Opdracht 1.2
		-- Maak in de sourcefile een aantal geometrische figuren aan (tenminste ´e´en van
		-- elke kleur), zodat je wat data krijgt, waarmee je kunt testen.

        testSquare = Square Red 4
        testRectangle = Rectangle Yellow 4 9
        testTriangle = Triangle Blue 5
        testCircle = Circle Red 12

		-- Opdracht 1.3
		-- Schrijf een functie, die een geometrisch object als parameter krijgt en de oppervlakte van het betreffende object uitrekent.

        calcArea:: Geofig -> Float
        calcArea (Square _ l) = l^2
        calcArea (Rectangle _ l w) = l*w
        calcArea (Triangle _ l) = ((l^2)*(sqrt 3)) / 4
        calcArea (Circle _ r) = pi * (r^2)

		-- Opdracht 1.4
		-- Schrijf een functie, die een geometrisch object als parameter krijgt en de ”omtrek” (=totale lengte van alle zijden) van het betreffende object uitrekent.

        calcCirc:: Geofig -> Float
        calcCirc (Square _ l) = l*4
        calcCirc (Rectangle _ l w) = (l*2)+(w*2)
        calcCirc (Triangle _ l) = l*3
        calcCirc (Circle _ r) = pi * (r*2)

        

        --Opdracht 1.5
		-- Schrijf een functie die een lijst van geometrische objecten als parameter krijgt
		-- en een lijst met alleen de Vierkanten teruggeeft. Schrijf soortgelijke functies
		-- voor de overige soorten objecten.

        listGeofigs = [testSquare, testRectangle, testTriangle, testCircle, Square Red 4, Square Blue 8, Circle Red 9]

        isSquare::Geofig -> Bool
        isSquare (Square _ _ ) = True
        isSquare _ = False

        findSquares:: [Geofig] -> [Geofig]
        findSquares [] = []
        findSquares geofigs = filter isSquare geofigs

        isRectangle::Geofig -> Bool
        isRectangle (Rectangle _ _ _) = True
        isRectangle _ = False

        findRectangles:: [Geofig] -> [Geofig]             
        findRectangles [] = []
        findRectangles geofigs = filter isRectangle geofigs

        isTriangle::Geofig -> Bool
        isTriangle (Triangle _ _ ) = True
        isTriangle _ = False

        findTriangles:: [Geofig] -> [Geofig]             
        findTriangles [] = []
        findTriangles geofigs = filter isTriangle geofigs

        isCircle::Geofig -> Bool
        isCircle (Circle _ _ ) = True
        isCircle _ = False

        findCircles:: [Geofig] -> [Geofig]             
        findCircles [] = []
        findCircles geofigs = filter isCircle geofigs


		-- Opdracht 1.6
		-- Schrijf een functie die een String en een lijst geometrische objecten als parameters krijgt. Indien de String de waarde ”Driehoek” heeft, geeft de functie
		-- alleen de driehoeken terug. Uiteraard werkt dit ook bij de overige geometrische
		-- objecten. Deze functie kan beschouwd worden als een iets algemenere versie
		-- van de functies geschreven in opdracht 5.
        
        findGeofigs::String -> [Geofig] -> [Geofig]
        findGeofigs _ [] = []
        findGeofigs "Square" geofigs = filter isSquare geofigs
        findGeofigs "Rectangle" geofigs = filter isRectangle geofigs
        findGeofigs "Triangle" geofigs = filter isTriangle geofigs
        findGeofigs "Circle" geofigs = filter isCircle geofigs




		-- opdracht 1.7: Schrijf een functie die een Kleur en een lijstje objecten als parameters heeft. De
		-- functie geeft een lijst terug van alleen di´e objecten met de betreffende kleur
        findColours::Colour -> [Geofig] -> [Geofig]
        findColours _ [] = []
        findColours colour geofigs = filter (\g -> geocolour g==colour) geofigs         ---als kleur zelfde is als gegeven kleur, true geven



        ---8: Opdracht 1.8
		-- Schrijf een functie die een lijstje objecten als parameter krijgt en het object met
		-- de grootste oppervlakte teruggeeft. Idem voor de grootste omtrek.

        findBiggestArea::[Geofig] -> (Geofig, Float)
        findBiggestArea geofigs = maximum (zip geofigs (map calcArea geofigs))      ---Bereken oppervlakte van elk element, in lijst zetten, en daar de max van pakken
           
        findBiggestCirc::[Geofig] -> (Geofig, Float)
        findBiggestCirc geofigs = maximum (zip geofigs (map calcCirc geofigs)) --Omtrek


        ---Opdracht 1.9
		-- Schrijf een functie die een geometrisch object aan een bestaande lijst toevoegt.

        addToGeofigList::Geofig -> [Geofig] --Adds Geofig to list of Geofigs
        addToGeofigList geofig = listGeofigs ++ [geofig]




	--Opdracht 1.10
	 	--10:Schrijf een functie die een lijst objecten als parameter krijgt. De functie geeft
		-- een lijst terug, waarin getallen staan. Ieder van deze getallen stelt de oppervlakte
		-- van het geometrisch figuur voor als percentage van de totale oppervlakte van
		-- alle figuren. Alle getallen bij elkaar opgeteld leveren derhalve de waarde 100 op.

        percAreaOfList::[Geofig] -> [Float]
        percAreaOfList geofigs = map percArea geofigs               ---voer percArea aan alle zaken in lijst uit
            where
                totArea = sum(map calcArea geofigs)                 ---totaal van alle oppervlaktes
                percArea g = ((calcArea g) / totArea)*100           ---berken oppervlake percentage van figuur tov totaal 


--- Opdracht 2
---Opdracht 2.1
        -- Schrijf het datatype Boek. Dit datatype bevat drie velden:
        -- • Prijs: De prijs van het boek.
        -- • Titel: De titel van het boek.
        -- • Auteur: De schrijver van het boek.
        -- Gebruik type synoniemen om de code leesbaar te houden.


        data Book = Book Price Title Author
            deriving (Show)

        type Price = Int
        type Title = String
        type Author = String


        -- Opdracht 2.2
        -- Leidt je Boek af van de typeclasses Eq en Ord. We willen boeken onderling
        -- kunnen vergelijken (Eq) en op volgorde kunnen zetten (Ord). Voorwaarden:
        -- • Twee boeken zijn aan elkaar gelijk als alle velden gelijk zijn.
        -- • Boeken dienen alfabetisch gesorteerd te kunnen worden op de titel van
        -- het boek. Prijs en auteur intesseren ons niet.
        instance Eq Book where
            (Book price1 title1 author1) == (Book price2 title2 author2) = (price1==price2) && (title1==title2) && (author1==author2)
            _ /= _ = False

        instance Ord Book where
            compare (Book _ title1 _) (Book _ title2 _) = compare title1 title2


        ---Opdracht 2.3
        -- Maak een lijst van tenminste vijf boeken. Test of je implementatie van de EQ
        -- en Ord typeclasses correct werkt door:
        -- • enkele boeken met elkaar te vergelijken.
        -- • de lijst met boeken te sorteren middels het gebruik van de sort functie.
        -- Deze functie zit in de library Data.List.

        book1 = Book 12 "Schaum's Outline of Programming with Java" "John R. Hubbard"
        book2 = Book 12 "The Subtle Art of Not Giving a F*ck" "Mark Manson"
        book3 = Book 87 "A Dance with Dragons" "Sjors R. R. Martijn" 
        book4 = Book 2 "Donald Duck" "Walt Disney"
        book5 = Book 8 "H. Potter" "J. R. R. Tolkien"
        book6 = Book 8 "H. Potter" "J. R. R. Tolkien" 
        -- troll

        bookList = [book1, book2, book3, book4, book5, book6]

        -- Tests:
        ---book1==book2 geeft False
        ---book1==book1 geeft True
        ---sort bookList geeft gesorteerde lijst list



        -- Opdracht 2.4
        -- Schrijf het datatype Box a. Dit datatype is algebra¨ısch. Een box:
        -- • is Leeg.
        -- • bevat iets, waarvan we van te voren niet weten wat het is.

        data Box a = EmptyBox
                | Box a 
                deriving (Show)


        --Opdracht 2.5
        -- Schrijf een functie die alle boeken in een gegeven lijst in een Box inpakt. Schrijf
        -- ook een functie die het omgekeerde doet en, so to speak, alle boeken weer
        -- uitpakt.
        putBooklistInBox:: [Book] -> Box [Book]
        putBooklistInBox booklist = Box booklist

        getBooklistFromBox:: Box [Book] -> [Book]
        getBooklistFromBox (Box booklist) = booklist

        ------------------------------------------------------------------------

        ---Opdracht 2.6
        -- Schrijf het datatype Zak. Ook dit datatype is algebra¨ısch. Verder gelden dezelfde voorwaarden als bij het Box type: Een zak bevat iets of is Leeg
        data Bag a = EmptyBag
                | Bag a
                deriving (Show)

        --Opdracht 2.7
        -- Leidt zowel het datatype Box als het datatype Zak af van de Functor typeclass
        -- en implementeer de functie fmap voor beide datatypen.

        instance Functor Box where
            fmap f EmptyBox = EmptyBox
            fmap f (Box a) = Box (f a)

        instance Functor Bag where
            fmap f EmptyBag = EmptyBag
            fmap f (Bag a) = Bag (f a)

        -- Opdracht 2.8
        -- Schrijf enkele oneliners:
        -- • Elk boek in de boekenlijst wordt ingepakt in een Zak en al deze zakken
        -- worden, per stuk, ingepakt in een Box
        -- • Maak een willekeurige lijst getallen. Stop elk getal in een Box. We hebben
        -- nu een lijst met boxes, waarbij in elke Box een getal zit.
        numberList = [0..100]

        putBooksInBagsAndBoxes:: [Book] -> [Box (Bag Book)]
        putBooksInBagsAndBoxes booklist =  map (fmap inBag) boxedList
                where
                    inBag a = Bag a
                    inBox a = Box a
                    boxedList = map inBox booklist
        
        putNumbersInBoxes:: Num a => [a] -> [Box a] 
        putNumbersInBoxes numlist = map inBox numlist
                where 
                    inBox a = Box a



        --Opdracht 2.9
        -- Schrijf het datatype List. Dit datatype is geparameteriseerd en definieert recursief een list.
        -- • Leidt de List af van de Functor typeclass.
        -- • Vul de List met dozen met getallen. Gebruik daarvoor de foldr of
        -- foldl functie.
        -- • Gebruik de functionaliteit van Functor om een met Box gevulde list te
        -- vervangen door een met Zak gevulde list.

        data List a = Head a (List a) | EmptyList 
                deriving (Show)
                
        instance Functor List where
            fmap f EmptyList = EmptyList
            fmap f (Head a tail) = Head (f a) (fmap f tail)        

        push :: a -> List a -> List a --Push een item naar een List
        push a EmptyList = Head a EmptyList
        push a (Head h tail) = Head a (Head h tail)

        pushList::List a -> [a] -> List a   --Push een List van items naar een List
        pushList EmptyList list = foldr push EmptyList list
        pushList (Head h tail) list = foldr push (Head h tail) list

        fillList lijst = foldr push EmptyList lijst 
            
        convertBoxToBag:: Box a -> Bag a
        convertBoxToBag (Box a) = Bag a

        convertBoxList::List (Box a) -> List (Bag a)   -- Lijst van dozen naar lijst van zakken verwisselen
        convertBoxList EmptyList = EmptyList
        convertBoxList list = fmap convertBoxToBag list