module Opdracht1
    where

        import Data.List

        ---1: Opdracht 1
		-- Definieer het datatype Geofig. Dit datatype stelt een geometriche figuur voor.
		-- Enkele eigenschappen:
		-- • Het datatype is algebra¨ısch: Er bestaan dus verschillende waarden (value
		-- constructoren) van. Deze zijn:
		-- – Vierkant: parameters: Lengte (=de lengte van een zijde)
		-- – Rechthoek: parameters: Lengte, Breedte
		-- – Driehoek: parameters: Lengte (=de lengte van een zijde), de driehoek is gelijkzijdig.
		-- – Cirkel: parameters: Straal
		-- • Elke geometrische figuur heeft een kleur. Mogelijke kleuren zijn: Rood, Blauw, Geel
		-- Ook voor de mogelijke kleuren wordt een nieuw datatype gedefinieerd,
		-- d.w.z. een enumeratietype.


      
        data Geofig = Square { 
                        geocolour::Colour, 
                        geolength::Float 
                        }
                    | Rectangle { 
                        geocolour::Colour,
                        geolength::Float,
                        geowidth::Float 
                        }
                    | Triangle {
                        geocolour::Colour,
                        geolength::Float
                        } 
                    | Circle {
                        geocolour::Colour,
                        georadius::Float
                        }
                    deriving (Eq, Ord, Show)

        data Colour = Red
                    | Blue
                    | Yellow
                    deriving (Eq, Ord, Show)


        ---Opdracht 2
		-- Maak in de sourcefile een aantal geometrische figuren aan (tenminste ´e´en van
		-- elke kleur), zodat je wat data krijgt, waarmee je kunt testen.

        testSquare = Square Red 4
        testRectangle = Rectangle Yellow 4 9
        testTriangle = Triangle Blue 5
        testCircle = Circle Red 12

		-- Opdracht 3
		-- Schrijf een functie, die een geometrisch object als parameter krijgt en de oppervlakte van het betreffende object uitrekent.

        calcArea:: Geofig -> Float
        calcArea (Square _ l) = l^2
        calcArea (Rectangle _ l w) = l*w
        calcArea (Triangle _ l) = ((l^2)*(sqrt 3)) / 4
        calcArea (Circle _ r) = pi * (r^2)

		-- Opdracht 4
		-- Schrijf een functie, die een geometrisch object als parameter krijgt en de ”omtrek” (=totale lengte van alle zijden) van het betreffende object uitrekent.

        calcCirc:: Geofig -> Float
        calcCirc (Square _ l) = l*4
        calcCirc (Rectangle _ l w) = (l*2)+(w*2)
        calcCirc (Triangle _ l) = l*3
        calcCirc (Circle _ r) = pi * (r*2)

        

        --Opdracht 5
		-- Schrijf een functie die een lijst van geometrische objecten als parameter krijgt
		-- en een lijst met alleen de Vierkanten teruggeeft. Schrijf soortgelijke functies
		-- voor de overige soorten objecten.

        listGeofigs = [testSquare, testRectangle, testTriangle, testCircle, Square Red 4, Square Blue 8, Circle Red 9]

        isSquare::Geofig -> Bool
        isSquare (Square _ _ ) = True
        isSquare _ = False

        findSquares:: [Geofig] -> [Geofig]
        findSquares [] = []
        findSquares geofigs = filter isSquare geofigs

        isRectangle::Geofig -> Bool
        isRectangle (Rectangle _ _ _) = True
        isRectangle _ = False

        findRectangles:: [Geofig] -> [Geofig]             
        findRectangles [] = []
        findRectangles geofigs = filter isRectangle geofigs

        isTriangle::Geofig -> Bool
        isTriangle (Triangle _ _ ) = True
        isTriangle _ = False

        findTriangles:: [Geofig] -> [Geofig]             
        findTriangles [] = []
        findTriangles geofigs = filter isTriangle geofigs

        isCircle::Geofig -> Bool
        isCircle (Circle _ _ ) = True
        isCircle _ = False

        findCircles:: [Geofig] -> [Geofig]             
        findCircles [] = []
        findCircles geofigs = filter isCircle geofigs


		-- Opdracht 6
		-- Schrijf een functie die een String en een lijst geometrische objecten als parameters krijgt. Indien de String de waarde ”Driehoek” heeft, geeft de functie
		-- alleen de driehoeken terug. Uiteraard werkt dit ook bij de overige geometrische
		-- objecten. Deze functie kan beschouwd worden als een iets algemenere versie
		-- van de functies geschreven in opdracht 5.
        
        findGeofigs::String -> [Geofig] -> [Geofig]
        findGeofigs _ [] = []
        findGeofigs "Square" geofigs = filter isSquare geofigs
        findGeofigs "Rectangle" geofigs = filter isRectangle geofigs
        findGeofigs "Triangle" geofigs = filter isTriangle geofigs
        findGeofigs "Circle" geofigs = filter isCircle geofigs




		-- opdracht 7: Schrijf een functie die een Kleur en een lijstje objecten als parameters heeft. De
		-- functie geeft een lijst terug van alleen di´e objecten met de betreffende kleur
        findColours::Colour -> [Geofig] -> [Geofig]
        findColours _ [] = []
        findColours colour geofigs = filter (\g -> geocolour g==colour) geofigs         ---als kleur zelfde is als gegeven kleur, true geven



        ---8: Opdracht 8
		-- Schrijf een functie die een lijstje objecten als parameter krijgt en het object met
		-- de grootste oppervlakte teruggeeft. Idem voor de grootste omtrek.

        findBiggestArea::[Geofig] -> (Geofig, Float)
        findBiggestArea geofigs = maximum (zip geofigs (map calcArea geofigs))      ---Bereken oppervlakte van elk element, in lijst zetten, en daar de max van pakken
           
        findBiggestCirc::[Geofig] -> (Geofig, Float)
        findBiggestCirc geofigs = maximum (zip geofigs (map calcCirc geofigs)) --Omtrek


        ---Opdracht 9
		-- Schrijf een functie die een geometrisch object aan een bestaande lijst toevoegt.

        addToGeofigList::Geofig -> [Geofig] --Adds Geofig to list of Geofigs
        addToGeofigList geofig = listGeofigs ++ [geofig]





	 	--10:Schrijf een functie die een lijst objecten als parameter krijgt. De functie geeft
		-- een lijst terug, waarin getallen staan. Ieder van deze getallen stelt de oppervlakte
		-- van het geometrisch figuur voor als percentage van de totale oppervlakte van
		-- alle figuren. Alle getallen bij elkaar opgeteld leveren derhalve de waarde 100 op.

        percAreaOfList::[Geofig] -> [Float]
        percAreaOfList geofigs = map percArea geofigs               ---voer percArea aan alle zaken in lijst uit
            where
                totArea = sum(map calcArea geofigs)                 ---totaal van alle oppervlaktes
                percArea g = ((calcArea g) / totArea)*100           ---berken oppervlake percentage van figuur tov totaal 



