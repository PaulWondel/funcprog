module Opdr3 where
import Data.Char
import Data.List

--Opdr1a
diff::(Double->Double)->Double->Double->Double
diff f p x = (f(x + p) - f(x)) / p

--Opdr1b
intr::(Double->Double)->Double->Double->Double->Double
intr f a b p =
    foldr (\l r->(dx * (f(a + (l * dx)))) + r) 0 [0..p-1]
    where dx = (b-a)/p

--Opdr2
dubbelen::(Ord a) => [a]->[a]
dubbelen s = sort(nub((s) \\ (nub s)))
