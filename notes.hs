module Voorbeeld where
	import Data.Char
	import Data.List 

--Haskell notes

9 +7 -- geeft 16

(+)9 7 -- geeft 16

++ -- Twee lijsten bij elkaar samenvoegen

:t ssee the nvr


telopt:Int->Int->Int
telop x y = x + y

let a = telop 4 5 -- plaats antwoord in variable
let a = telop 4   --  geeft geen error bij compilen maar wel als a wordt gevraagd om te tonen

	INT	->	INT	->	INT
--	a(input)	b(input)	return
(INT->INT)->INT->INT


neg::Int->Int
neg x
	|x<0=x
	|otherwise =-x

apply::(Int->Int)->[Int]->[Int]
apply f []=[]
apply f (x:xs) = f x apply : apply f xs

zipWith::(a->b->c)->[a]->[b]->[c]

zipWith (+) [1..5] [10,20,30,40,50] --geeft -> [11,22,33,44,55]

zipWith (\x y -> (x,y)) [1..5] [10,20,30,40,50]
zipWith (\x y -> [x,y]) [1..5] [10,20,30,40,50]


third::(a,b,c)->c
third (_,_,c) = c -- _ geeft aan dat we die parameters niet willen gebruiken


egcd::Int->Int->(Int,Int,Int)
mid(_,b,_)=b

megcd::Int->Int->Int
megcdab=mid(egcd a b)

restklasse::Interger->Interger->Interger
restklasse start modulus = [start + (n*modules)|n<-[1..]]

restklasse2::Int->Int->[Int]
restklasse2 start modulus = [start,start+modulus]

mijnegcd e m = let d = mid $ egcd e m in

mijnegcd e m
    |d<0 = d+m
    |otherswise = d
    where d = mid $egcd e m

-- totient n^e = n^(e-1) *(n-1)

m = p*q
m'=(p-1)(q-1)
e=43
d=egcd e m' 

-----------------------------------------------------------------------------------------------------------
-- Func Prog 2

module Main where

	import Data.Char
	import System.IO

	main = 
	
	data Doos a = Leeg | Doos a deriving Show

