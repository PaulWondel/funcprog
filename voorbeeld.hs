module Voorbeeld where
	
	--Pattern Matching
	faca::Int->Int
	faca 0 = 1
	faca n = n* faca (n-1)

	--Guards
	facb::Int->Int
	facb n
		|n==0 = 1
		|otherwise = n* facb (n-1)

	--Nulpunten tweedegraadsfunctie
	nulpuntena::Double->Double->Double->[Double]
	nulpuntena a b c = [(-b+sqrt((b^2)-4*a*c))/(2*a),(-b-sqrt((b^2)-4*a*c))/(2*a)]
	
	--Met guards
	nulpuntenb::Double->Double->Double->[Double]
	nulpuntenb a b c
		| disc < 0 = [0,0,0,0,0,0,0,0] 
		| disc == 0 = [formulaa]
		| disc > 0 = [formulaa, formulab]
		where disc = ((b^2)-4*a*c)
		      formulaa = ((-b - sqrt(disc)) / 2*a)
		      formulab = ((-b + sqrt(disc)) / 2*a)
	
		
	--Dobbelstenen
	dobbelstenena::Int
	dobbelstenena = length[(x,y,z)|x<-[1..6],y<-[1..6],z<-[1..6], mod (x+y+z) 5 == 0]

	dobbelstenenb::Int -> Int
	dobbelstenenb n = length[(x,y,z)|x<-[1..6],y<-[1..6],z<-[1..6], mod (x+y+z) n == 0]
